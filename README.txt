README
------

Provides a set of blocks for the WhatCounts (http://whatcounts.com) Web API
service, to allow users to subscribe/unsubscribe from lists on your WhatCounts
account.

Requirements
------------

This module requires Drupal 5.x.

Installation
------------

See INSTALL.txt

Contributors
------------
Derek Laventure <derek@anarres.ca>

Sponsored by: rabble.ca

